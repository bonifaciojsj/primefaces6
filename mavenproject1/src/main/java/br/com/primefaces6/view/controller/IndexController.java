package br.com.primefaces6.view.controller;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.ItemSelectEvent;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author bonifácio
 */
@ViewScoped
@Named
public class IndexController implements Serializable {

    private int step = 3;

    private Date data;

    private String value;
    private String value1;
    private List<String> values;

    private BarChartModel barModel;
    private HorizontalBarChartModel horizontalBarModel;
    private PieChartModel pieModel1;
    private LineChartModel lineModel1;
    private MeterGaugeChartModel meterGaugeModel1;

    @PostConstruct
    public void init() {
        values = new ArrayList<>();
        values.add("ae1");
        values.add("ae2");
        values.add("ae3");
        values.add("ae3");
        createBarModels();
        createPieModels();
        createLineModels();
        createMeterGaugeModels();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public List<DayOfWeek> getCars() {
        return Arrays.asList(DayOfWeek.values());
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public void add() {
        values.add(value);
        value = "";
    }

    public String getTeste() {
        return "ae123";
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public LineChartModel getLineModel1() {
        return lineModel1;
    }

    private void createPieModels() {
        createPieModel1();
    }

    public MeterGaugeChartModel getMeterGaugeModel1() {
        return meterGaugeModel1;
    }

    private MeterGaugeChartModel initMeterGaugeModel() {
        List<Number> intervals = new ArrayList<Number>() {
            {
                add(20);
                add(50);
                add(120);
                add(220);
            }
        };

        return new MeterGaugeChartModel(140, intervals);
    }

    private void createMeterGaugeModels() {
        meterGaugeModel1 = initMeterGaugeModel();
        meterGaugeModel1.setTitle("MeterGauge Chart");
        meterGaugeModel1.setGaugeLabel("Valor");
        meterGaugeModel1.setLabelHeightAdjust(110);
        meterGaugeModel1.setShowTickLabels(false);
        meterGaugeModel1.setIntervalOuterRadius(170);
//         
//        meterGaugeModel2.setTitle("Custom Options");
//        meterGaugeModel2.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
//        meterGaugeModel2.setGaugeLabel("km/h");
//        meterGaugeModel2.setGaugeLabelPosition("bottom");
//        meterGaugeModel2.setShowTickLabels(false);
//        meterGaugeModel2.setLabelHeightAdjust(110);
//        meterGaugeModel2.setIntervalOuterRadius(100);
    }

    private void createLineModels() {
        lineModel1 = initLinearModel();
        lineModel1.setTitle("Linear Chart");
        lineModel1.setLegendPosition("e");
        lineModel1.setShowPointLabels(false);
        Axis yAxis = lineModel1.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10);
        lineModel1.setExtender("customExtenderLine");
        lineModel1.setTitle("<div style='height: 40px; text-align: left; padding-left: 20px; color:#757575;'>Bar Chart</div>");
        lineModel1.setSeriesColors("4285f4,db4437,f4b400,016c59");
        lineModel1.setShadow(false);
        lineModel1.setDatatipFormat("<table class='jqplot-tooltip-content'><thead><tr><th>%s</th></tr></thead><tbody><td>%s</td><td>%s EUR</td></tbody></table>");
    }

    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setShowMarker(false);
        series1.setLabel("Series 1");

        series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);

        LineChartSeries series2 = new LineChartSeries();
        series2.setShowMarker(false);
        series2.setLabel("Series 2");

        series2.set(1, 6);
        series2.set(2, 3);
        series2.set(3, 2);
        series2.set(4, 7);
        series2.set(5, 9);

        LineChartSeries series3 = new LineChartSeries();
        series3.setShowMarker(false);
        series3.setLabel("Series 3");

        series3.set(1, 12);
        series3.set(2, 7);
        series3.set(3, 5);
        series3.set(4, 4);
        series3.set(5, 2);

        model.addSeries(series1);
        model.addSeries(series2);
        model.addSeries(series3);

        return model;
    }

    private void createPieModel1() {
        pieModel1 = new PieChartModel();

        pieModel1.set("Brand 1", 540);
        pieModel1.set("Brand 2", 325);
        pieModel1.set("Brand 3", 702);
        pieModel1.set("Brand 4", 421);

        pieModel1.setExtender("customExtender");
        pieModel1.setTitle("<div style='height: 40px; text-align: left; padding-left: 20px; color:#757575;'>Bar Chart</div>");
        pieModel1.setSeriesColors("4285f4,db4437,f4b400,016c59");
        pieModel1.setShadow(false);
        pieModel1.setDatatipFormat("<table style='background-color: #fff; padding: 8px; font-size: 16px; box-shadow: 1px 1px 1px 2px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);'><thead><tr><th>Date</th></tr></thead><tbody><td>%s</td><td>%s EUR</td></tbody></table>");

        pieModel1.setLegendPosition("w");
    }

    public boolean filterByString(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        return ((Comparable) value).compareTo(Integer.valueOf(filterText)) > 0;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public HorizontalBarChartModel getHorizontalBarModel() {
        return horizontalBarModel;
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 120);
        boys.set("2005", 100);
        boys.set("2006", 44);
        boys.set("2007", 150);
        boys.set("2008", 25);

        ChartSeries boys2 = new ChartSeries();
        boys2.setLabel("Boys2");
        boys2.set("2004", 50);
        boys2.set("2005", 96);
        boys2.set("2006", 44);
        boys2.set("2007", 55);
        boys2.set("2008", 25);

        ChartSeries boys3 = new ChartSeries();
        boys3.setLabel("Boys3");
        boys3.set("2004", 50);
        boys3.set("2005", 96);
        boys3.set("2006", 44);
        boys3.set("2007", 55);
        boys3.set("2008", 25);

        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 110);
        girls.set("2007", 135);
        girls.set("2008", 120);

        model.addSeries(boys);
        model.addSeries(boys2);
        model.addSeries(boys3);
        model.addSeries(girls);

        return model;
    }

    private void createBarModels() {
        createBarModel();
        createHorizontalBarModel();
    }

    private void createBarModel() {
        barModel = initBarModel();
        barModel.setExtender("customExtender");

        barModel.setTitle("<div style='height: 40px; text-align: left; padding-left: 20px; color:#757575;'>Bar Chart</div>");
        barModel.setLegendPosition("nw");
        barModel.setSeriesColors("4285f4,db4437,f4b400,016c59");
        barModel.setShadow(false);
        barModel.setDatatipFormat(
                "  <div class='jqplot-tooltip-content'>"
                + " <div class='content'>"
                + "  <div class='serie'>%s</div>"
                + "  <div class='detail'>"
                + "    <div class='label'>%d</div>"
                + "    <div class='value'>R$ %s</div>"
                + "  </div>"
                + " </div>"
                + "</div>");
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Gender");

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Births");
        yAxis.setMin(0);
        yAxis.setMax(200);
    }

    private void createHorizontalBarModel() {
        horizontalBarModel = new HorizontalBarChartModel();
        horizontalBarModel.setExtender("customExtender");
        horizontalBarModel.setSeriesColors("4285f4,db4437,f4b400,016c59");
        horizontalBarModel.setShadow(false);

        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 25);

        ChartSeries boys2 = new ChartSeries();
        boys2.setLabel("Boys2");
        boys2.set("2004", 75);

        ChartSeries boys3 = new ChartSeries();
        boys3.setLabel("Boys3");
        boys3.set("2004", 11);

        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 37);

        horizontalBarModel.addSeries(boys);
        horizontalBarModel.addSeries(boys2);
        horizontalBarModel.addSeries(boys3);
        horizontalBarModel.addSeries(girls);

        horizontalBarModel.setTitle("Horizontal and Stacked");
        horizontalBarModel.setLegendPosition("e");

        Axis xAxis = horizontalBarModel.getAxis(AxisType.X);
        xAxis.setLabel("Births");
        xAxis.setMin(0);
        xAxis.setMax(200);

        Axis yAxis = horizontalBarModel.getAxis(AxisType.Y);
        yAxis.setLabel("Gender");
    }

    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
