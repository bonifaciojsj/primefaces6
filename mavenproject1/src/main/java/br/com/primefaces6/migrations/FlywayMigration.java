package br.com.primefaces6.migrations;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.flywaydb.core.Flyway;

/**
 *
 * @author José
 */
@Startup
@Singleton
public class FlywayMigration {
    
    @PostConstruct
    public void init() {
        System.out.println("teste1:");
        Flyway flyway = new Flyway();
        
        flyway.setDataSource("jdbc:postgresql://localhost:5432/teste", "postgres", "sql", null);
        flyway.migrate();
        System.out.println("teste2:");
    }
}
